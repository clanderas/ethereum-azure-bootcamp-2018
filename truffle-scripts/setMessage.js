const argv = require('yargs').argv;

const SimpleContract = artifacts.require('SimpleContract');

module.exports = async function () { 
    const account = web3.eth.accounts[0];   
    let instance = await SimpleContract.deployed();
    const {tx} = await instance.setMessage(argv.message, {
        from: account,
        value: web3.toWei("2.1", 'ether')
    });

    console.log(`Transaction hash is ${tx}`);
}

