const argv = require('yargs').argv;

module.exports = async function () { 
    const tx = await web3.eth.getTransaction(argv.tx);    
    const receipt = await web3.eth.getTransactionReceipt(argv.tx);
    console.log(`Transaction value is ${tx.value.toNumber()}`);
    console.log(`Transaction value in ether is: ${web3.fromWei(tx.value.toNumber(), 'ether')}`);
    console.log(`Hexadecimal input is ${tx.input}`);
    console.log(`Ascii input is ${web3.toAscii(tx.input).trim()}`);    
}

