
var SimpleContract = artifacts.require('SimpleContract');

module.exports = async function () { 
    let instance = await SimpleContract.deployed();
    const message = await instance.getMessage();
    console.log(`Message is: ${message}`); 
    console.log(`Balance is: ${await instance.getBalanceInEther()} ether`);  
}

