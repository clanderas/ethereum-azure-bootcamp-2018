const path = require('path');
const fs = require('fs');
const solc = require('solc');
const chalk = require('chalk');

const contractPath = path.resolve(__dirname, "../contracts", "SimpleContract.sol");
const source = fs.readFileSync(contractPath, 'utf8');

const {interface, bytecode} = solc.compile(source, 1).contracts[':SimpleContract'];

console.log(chalk.cyan(bytecode));
console.log(chalk.green(interface));

module.exports = {interface, bytecode};
