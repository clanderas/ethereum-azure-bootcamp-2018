const Web3 = require('web3');
const { interface, bytecode } = require('./compile');

const provider = new Web3.providers.HttpProvider("http://bc27sz6h4s7y.westeurope.cloudapp.azure.com:8545 ");

const web3 = new Web3(provider);

const deploy = async () => {
    const accounts = await web3.eth.getAccounts();
    
    console.log(await web3.eth.getBalance(accounts[0]));
    console.log('Starting to deploy contract from account', accounts[0]);

    const contract = await new web3.eth.Contract(JSON.parse(interface))
        .deploy({ data: "0x" + bytecode.toString(), arguments: ['initialMessage'] })
        .send({ from: accounts[0], gas: '1000000' });    
        
    console.log(interface);
    console.log('Contract deployed to', contract.options.address);
};

deploy();