pragma solidity ^0.4.17;

contract Airline {
    
    struct Customer { 
        string name;
        address addr;
        uint loyaltyPoints;
        uint totalFlights;
    }
    
    uint etherPerPoint = 0.5e18;

    mapping(address => Customer) public customersMapping;
    address[] public customers;
    
    event AddedLoyaltyMember(address indexed newMember);
    event FlightPurchased(address indexed customer, uint price);
    
    address public owner;
    
    function Airline() public {
        owner = msg.sender;
    }
    
    function newCustomer(string name) public {
        
        Customer storage customer = customersMapping[msg.sender];
        customer.name = name;
        customer.addr = msg.sender;
        customer.loyaltyPoints += 1;
        
        customers.push(customer.addr);
        
        AddedLoyaltyMember(msg.sender);
    }
    
    function getCustomer(string name) public view returns (string customerName, uint loyaltyPoints, uint totalFlights){
        
        Customer memory customer = Customer("",0,0, 0x0);
        
        for(uint i = 0 ; i < customers.length; i++){
            Customer memory current = customersMapping[customers[i]];
            if(keccak256(current.name) == keccak256(name)){
                customer = current;
                break;
            }
        }
        return (customer.name, customer.loyaltyPoints, customer.totalFlights);
    }    
    
    function buyFlight() public payable {
        //simulate flight purchase and assign more loyaltyPoints
        require(msg.value == 20 ether);
        Customer storage customer = customersMapping[msg.sender];
        customer.loyaltyPoints += 5;
        customer.totalFlights += 1;
        
        FlightPurchased(msg.sender, 2);
    }
    
    function redeemLoyaltyPoints() public {
        Customer storage customer = customersMapping[msg.sender];
        uint etherToRefund = etherPerPoint * customer.loyaltyPoints;
        msg.sender.transfer(etherToRefund);
        customer.loyaltyPoints = 0;
    }
    
    function getRefundableEther() public view returns (uint) {
        return etherPerPoint * customersMapping[msg.sender].loyaltyPoints;   
    }
    
    function getAirlineBalance() public view isOwner returns (uint) {
        address airlineAddr = this;
        return airlineAddr.balance;
    }
    
    function getAirlineBalanceInEther() public view isOwner returns (uint){
        address airlineAddr = this;
        return airlineAddr.balance / 1e18;
    }
    
    function getTotalCustomers() public view returns (uint) {
        return customers.length;
    }
    
    modifier isOwner() { 
        require(owner == msg.sender);
        _;
    }
}