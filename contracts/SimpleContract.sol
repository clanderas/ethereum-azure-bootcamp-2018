pragma solidity ^0.4.19;
contract SimpleContract {
    
    address public owner;
    string private _message;

    function SimpleContract(string message) public {
        owner = msg.sender;
        _message = message;
    }
    
    function getMessage() public view returns (string) {
        return _message;
    }
    
    function setMessage(string newMessage) public payable {
        require(msg.value >= 2 ether);
        _message = newMessage;
    }
    
    function getBalanceInEther() public view returns (uint) {
        address simpleContract = this;
        return simpleContract.balance / 1e18;        
    }
}